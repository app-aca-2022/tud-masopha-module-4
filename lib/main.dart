import 'package:flutter/material.dart';
import 'package:tud_masopha_module_3/screens/edit_user_screen.dart';
import 'package:tud_masopha_module_3/screens/login_screen.dart';
import 'package:tud_masopha_module_3/screens/register_screen.dart';

import 'screens/home.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LoginScreen(),
      theme: ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.yellow,
      ),
    ); 
  }
}