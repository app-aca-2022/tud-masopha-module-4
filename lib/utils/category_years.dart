import 'package:flutter/material.dart';

class AppCategory extends StatelessWidget {
  final String appCategory;
  final int winningYear;
  final bool isSelected;
  final VoidCallback onTap;

  AppCategory({
    required this.appCategory, 
    required this.winningYear, 
    required this.isSelected, 
    required this.onTap,    
    });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.only(left:25.0),
        child: Text(
          appCategory,
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
            color:isSelected ? Colors.yellow : Colors.white,
          ),
        ),
      ),
    );

    
  }
}