import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:tud_masopha_module_3/screens/add_screen.dart';

import '../utils/category_years.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int selectedIndex = 0;
  // list of apps categories
  final List appCategory = [
    ['Consumer Solution', true,],
    ['Educational Solution',false],
    ['Enterprise Solution',false],
    ['Innovative Solution',false],
    ['Gaming Solution',false],
    ['Health Solution',false],
    ['Financial Solution',false],
    ['Agricultural Solution',false],
  ];

  // List of Years Since start
  final winningYear = [2012,2013,2014,2015,
         2016,2017,2018,2019,
         2019,2020,2021,
  ];

  //App Category Selected Methods
  void appCategorySelected(int index){
    setState(() {

      //for loop makes every selection false
      for (int i = 0; i <appCategory.length; i++){
        appCategory[i][1] = false;
      }
      appCategory[index][1] = true;
    });
  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Center(child: Text('Home')),
      ),
      
      // notched floating button
      floatingActionButton:FloatingActionButton( //Floating action button on Scaffold
         onPressed: (){
          //code to execute on button press
          Navigator.push(context,MaterialPageRoute(builder: (context) => AddApp()),);
           },
       child: Icon(Icons.add),
          backgroundColor: Colors.yellow, //icon inside button
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,

      bottomNavigationBar: BottomAppBar(
             shape: CircularNotchedRectangle(), 
             //shape of notch
             notchMargin: 8,
        child: BottomNavigationBar(
          backgroundColor: Colors.transparent,
          selectedItemColor: Colors.yellow,
          unselectedItemColor: Colors.white60,
          currentIndex: selectedIndex,
          onTap: (index) => setState(() =>  selectedIndex = index),          
            items: [
              BottomNavigationBarItem(
                icon: Icon
              (Icons.home),
              label: '',
              ),
              BottomNavigationBarItem(
                icon: Icon
                (Icons.person),
              label: '',
              ),
            ],
        ),
      ),
      body:
      Column( children: [
          //Find Apps that won
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25.0),
            child: Row(
              children: [
                Container(
                  height: 30.0,
                  width: 30.0,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                        'logos/aoy.jpg'),
                      fit: BoxFit.fill,
                    ),
                    shape: BoxShape.circle,
                  ),
                ),
                SizedBox(width:10),
                Flexible(
                  child: Text(
                    'MTN App of The Year Winners',
                    style: TextStyle(
                     fontSize: 20,
                    ),
                  ),
                ),
              ],
            ),
          ),
      
          SizedBox(height:25),
      
          // Search
          Padding(
            padding: const EdgeInsets.symmetric(horizontal:25.0),
            child: TextField(
              decoration: InputDecoration(
                prefixIcon: Icon(Icons.search),
                hintText: 'Find Winning Apps..',
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey.shade600),
                ),
                enabledBorder:  OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey.shade600),
                ),
              ),
            ),
          ),
      
          SizedBox(height: 20),
      
          //Categories and Years
           Container(
            height: 50,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: appCategory.length,
              itemBuilder: (context, index) {
              return AppCategory(
                appCategory: appCategory[index][0],
                isSelected: appCategory[index][1],
                winningYear: winningYear[index],
                onTap: (){
                  appCategorySelected(index);
                },              
              );
            })),
           
      
      
          //App tiles        
          Flexible(
            child: 
            GridView.count(
      
              crossAxisCount: 2,
              mainAxisSpacing: 8,
              crossAxisSpacing: 8,
              children: [
                
                InkWell(
      
                  onTap: (){
                    showAppDetails(
                      context, 
                      'logos/Ambani-Afrika.jpg', 
                      'Ambani Afrika', 
                      'Educational Solution', 
                      '2021');
                  },
                  child: Container(
                    height: 150.0,
                    width: 150.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                        image: AssetImage('logos/Ambani-Afrika.jpg'),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
      
                InkWell(
                  onTap: (){
                    showAppDetails(
                      context, 
                      'logos/Takealot.jpg', 
                      'Takealot', 
                      'Enterprise Solution', 
                      '2021');
                  },
      
                  child: Container(
                    height: 150.0,
                    width: 150.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                        image: AssetImage('logos/Takealot.jpg'),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
                
                InkWell(
                  onTap: (){
                    showAppDetails(
                      context, 
                      'logos/Shytft.jpg', 
                      'Shyft', 
                      'Financial Solution', 
                      '2021');
                  },
      
                  child: Container(
                    height: 150.0,
                    width: 150.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                        image: AssetImage('logos/Shytft.jpg'),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
                
                InkWell(
                  onTap: (){
                    showAppDetails(
                      context, 
                      'logos/SISA.jpg', 
                      'SISA', 
                      'Health Solution', 
                      '2021');
                  },
      
                  child: Container(
                    height: 150.0,
                    width: 150.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                        image: AssetImage('logos/SISA.jpg'),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
      
                InkWell(
                  onTap: (){
                    showAppDetails(
                      context, 
                      'logos/Guardian-Health-Platform.jpg', 
                      'Guardian Health Platform', 
                      'Health Solution', 
                      '2021');
                  },
      
                  child: Container(
                    height: 150.0,
                    width: 150.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                        image: AssetImage('logos/Guardian-Health-Platform.jpg'),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
      
                InkWell(
                  onTap: (){
                    showAppDetails(
                      context, 
                      'logos/Murimi.jpg', 
                      'Murimi', 
                      'Agricultural Solution', 
                      '2021');
                  },
      
                  child: Container(
                    height: 150.0,
                    width: 150.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                        image: AssetImage('logos/Murimi.jpg'),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
                
                InkWell(
                  onTap: (){
                    showAppDetails(
                      context, 
                      'logos/UniWise.jpg', 
                      'UniWise', 
                      'Campus Cup Solution', 
                      '2021');
                  },
      
                  child: Container(
                    height: 150.0,
                    width: 150.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                        image: AssetImage('logos/UniWise.jpg'),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
                
                InkWell(
                  onTap: (){
                    showAppDetails(
                      context, 
                      'logos/Kazi-App.jpg', 
                      'Kazi App', 
                      'African Solution', 
                      '2021');
                  },
      
                  child: Container(
                    height: 150.0,
                    width: 150.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                        image: AssetImage('logos/Kazi-App.jpg'),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
      
                InkWell(
                  onTap: (){
                    showAppDetails(
                      context, 
                      'logos/Rekindle.jpg', 
                      'Rekindle Learning', 
                      'Women in STEM Category', 
                      '2021');
                  },
      
                  child: Container(
                    height: 150.0,
                    width: 150.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                        image: AssetImage('logos/Rekindle.jpg'),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
      
                InkWell(
                  onTap: (){
                    showAppDetails(
                      context, 
                      'logos/Hellopay-SoftPOS.jpg', 
                      'Hellopay POS', 
                      'Most Innovative Solution', 
                      '2021');
                  },
      
                  child: Container(
                    height: 150.0,
                    width: 150.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                        image: AssetImage('logos/Hellopay-SoftPOS.jpg'),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
                
                InkWell(
                  onTap: (){
                    showAppDetails(
                      context, 
                      'logos/Roadsave.jpg', 
                      'Road Save', 
                      'Huawei Category', 
                      '2021');
                  },
      
                  child: Container(
                    height: 150.0,
                    width: 150.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                        image: AssetImage('logos/Roadsave.jpg'),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
                                
                
              ],
            ),
          ),   
        ]),
    );
    
  }
}

showAppDetails(context, img, name, category, year){
  return showDialog(
    context: context, 
    builder: (context) {
      return Center(
        child: Material(
          type: MaterialType.transparency,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.grey[600],
            ),
            padding: EdgeInsets.all(10),
            width: MediaQuery.of(context).size.width * 0.8,
            height: 420,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: Image.asset(
                    img,
                    fit: BoxFit.cover,
                    ),
                ),
                SizedBox(height: 10,),
                
                    Text(
                      name,
                      style: TextStyle(
                        fontSize: 25,
                        color: Colors.white,
                        fontWeight: FontWeight.bold
                      )
                    ),
                    Text(
                      year,
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.yellow,
                      )                      
                    ),
                  
                SizedBox(height: 10,),
                Text(
                  category,
                  style: TextStyle(
                    fontSize:15,
                    color: Colors.grey[500],
                    ),

                )
              ],
            ),
          ),
        ),
      );
    }
  );
}