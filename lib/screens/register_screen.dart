import 'package:flutter/material.dart';
import 'package:tud_masopha_module_3/screens/login_screen.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreen createState() => _RegisterScreen();
}

class _RegisterScreen extends State<RegisterScreen> {
  bool showPassword = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:
        Text('New User Registration',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.yellow,
            fontSize: 14,
          ),
        ),
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        elevation: 1,
        leading: 
        IconButton(
          icon: Icon(
            Icons.arrow_back,
          color: Colors.yellow,
          ),
          onPressed: () {
            Navigator.pop(
              context, MaterialPageRoute(
                builder: (context) => LoginScreen()
                ),
                );
          },
        ),
        actions: [
          IconButton(
          icon: Icon(
            Icons.settings,
          color: Colors.yellow,
          ),
          onPressed: () {},
        ),

        ],
      ),
      body: Container(
        padding: EdgeInsets.only(left: 16, top: 25, right: 16),
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: ListView(
            children: [
              SizedBox(
                height: 15,
              ),
              // Center(
              //   child: Stack(
              //     children: [
              //       Container(
              //         width: 130,
              //         height: 130,
              //         decoration: BoxDecoration(
              //             border: Border.all(
              //                 width: 4,
              //                 color: Theme.of(context).scaffoldBackgroundColor),
              //             boxShadow: [
              //               BoxShadow(
              //                   spreadRadius: 2,
              //                   blurRadius: 10,
              //                   color: Colors.black.withOpacity(0.1),
              //                   offset: Offset(0, 10))
              //             ],
              //             shape: BoxShape.circle,
              //             image: DecorationImage(
              //                 fit: BoxFit.cover,
              //                image: AssetImage('pimages/lion.jpg'),)),
              //       ),
              //       Positioned(
              //           bottom: 0,
              //           right: 0,
              //           child: Container(
              //             height: 40,
              //             width: 40,
              //             decoration: BoxDecoration(
              //               shape: BoxShape.circle,
              //               border: Border.all(
              //                 width: 4,
              //                 color: Theme.of(context).scaffoldBackgroundColor,
              //               ),
              //               color: Colors.green,
              //             ),
              //             child: Icon(
              //               Icons.edit,
              //               color: Colors.white,
              //             ),
              //           )),
              //     ],
              //   ),
              // ),
              SizedBox(
                height: 35,
              ),
              buildTextField("Full Name", "TUD Masopha", false),
              buildTextField("E-mail", "tudesign.sa@gmail.com", false),
              buildTextField("Password", "********", true),
              buildTextField("Location", "Soweto, Batho", false),
              SizedBox(
                height: 25,
              ),
              
                
                  // Cancel Button
                             
              // Save Button
                  Padding(
               padding: const EdgeInsets.symmetric(horizontal:25.0),
               child: Container(
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(color: Colors.yellow,
                borderRadius: BorderRadius.circular(10),
                ),
                child: Center(
                  child: Text(
                    'SAVE',
                    style: TextStyle(
                      letterSpacing: 8,
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      color: Colors.black,
                    ),
                    ),
                ),
               ),
              ),
                ],
              )
            
          ),
        ),
      );
    
  }

  Widget buildTextField(
      String labelText, String placeholder, bool isPasswordTextField) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 35.0),
      child: TextField(
        obscureText: isPasswordTextField ? showPassword : false,
        decoration: InputDecoration(
            suffixIcon: isPasswordTextField
                ? IconButton(
                    onPressed: () {
                      setState(() {
                        showPassword = !showPassword;
                      });
                    },
                    icon: Icon(
                      Icons.remove_red_eye,
                      color: Colors.yellow,
                    ),
                  )
                : null,
            contentPadding: EdgeInsets.only(bottom: 3),
            labelText: labelText,
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: placeholder,
            hintStyle: TextStyle(
              fontSize: 16,
              color: Colors.white,
            )),
      ),
    );
  }
}